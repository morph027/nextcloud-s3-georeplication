-- https://dba.stackexchange.com/questions/198840/setting-up-replication-with-mariadb-10-3-4-docker-images

-- drop the application user
DROP USER IF EXISTS 'nextcloud'@'%';

-- drop the root user
DROP USER IF EXISTS 'root'@'localhost';

-- configure the connection to the master
--
CHANGE MASTER TO
    MASTER_HOST='primary_db_1',
    MASTER_USER='replicant',
    MASTER_PASSWORD='password',
    MASTER_PORT=3306,
    MASTER_USE_GTID=slave_pos,
    MASTER_CONNECT_RETRY=10;

-- start the slave
--
START SLAVE;
