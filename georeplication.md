<!-- $theme: gaia -->
<!-- template: gaia -->
<!-- $width: 2560 -->
<!-- $height: 1440-->

# Simple Nextcloud Georeplication

---

### Idea

The idea came from a twitter user asking for something like this.
Let's assume companies might bigger budgets for infrastructure, so there a replicated storage and/or database cluster might be present.

This is targeted towards private users to achive the goal with minimal effort and technical understanding for complicated cluster setups.

The setup will be active/passive, means the backup is not usable until it's promoted to be the new instance.

---

### How?

* Database: [MariaDB Replication](https://mariadb.com/kb/en/library/setting-up-replication/)
* Files: [Minio S3 server](https://minio.io) + Minio S3 client ==mc== which watches and mirrors buckets between servers

---

### Alternatives

#### Database

* cron job which dumps database to replicated S3 ([even incremental](https://www.percona.com/doc/percona-xtrabackup/2.1/xtrabackup_bin/incremental_backups.html))
* [MariaDB Galera cluster](https://mariadb.com/kb/en/the-mariadb-library/getting-started-with-mariadb-galera-cluster/) (requires at least 3 nodes)

#### Files

* simple rsync via cron
* underlying ZFS datasets and send/recv incremental snapshots
* Ceph Cluster
* ... many more ;)

---

### Lab test setup

We will use Docker to setup all the components:

* Nextcloud (Apache2 + PHP + ....)
* Database + Replication
* S3 Server
* S3 Sync

In a real world example, you're probably already running your Nextcloud and "just" need to add the second instance.
This lab will setup a fresh Nextcloud without migrating files.

---

* create a seperate docker network: `docker network create nextcloud`
* start the primary instance: `docker-compose -f primary.yml -p primary up -d`
* start the secondary instance: `docker-compose -f secondary.yml -p secondary up -d`
* visit http://localhost, but **DO NOT COMPLETE THE SETUP YET!**
* get a copy of the current config file: `docker cp primary_nextcloud_1:/var/www/html/config/config.php .`

---

* edit `config.php` and add the objectstore section:
```php
  'objectstore' => array(
    'class' => 'OC\\Files\\ObjectStore\\S3',
      'arguments' => array(
        'bucket' => 'nextcloud',
        'autocreate' => true,
        'key'    => 'YK2CGOAK1JGCCRB43IND',
        'secret' => 'r+DKuXH0y53pi2egmAJP52z1Fgl23EAAj70aJujb',
        'hostname' => 's3',
        'port' => 9000,
        'use_ssl' => false,
        'region' => 'optional',
        'use_path_style'=>true
      ),
  ),
```

---

* overwrite container config: `docker cp config.php primary_nextcloud_1:/var/www/html/config/config.php`
* adjust file ownership: `docker exec primary_nextcloud_1 chown www-data:root /var/www/html/config/config.php`
* finish the setup at http://localhost

---

* get a copy of the final config file: `docker cp primary_nextcloud_1:/var/www/html/config/config.php .`
* overwrite secondary container config: `docker cp config.php secondary_nextcloud_1:/var/www/html/config/config.php`
* adjust file ownership: `docker exec secondary_nextcloud_1 chown www-data:root /var/www/html/config/config.php`
* initialize secondarys data dir: `docker exec secondary_nextcloud_1 install -o www-data -g root /dev/null /var/www/html/data/.ocdata`
* fix *.htaccess*: `docker exec secondary_nextcloud_1 sh -c 'exec su www-data -s /bin/bash -c "php -f /var/www/html/occ maintenance:update:htaccess"'`

---

### Failover

* stop the primary instance: `docker-compose -f primary.yml -p primary down`
* stop the secondary instance: `docker-compose -f secondary.yml -p secondary down`
* backup old primary data (only in this lab, the real second machine would not have primary data): `sudo mv data/primary data/primary.bak`
* use replicated secondary data as primary: `sudo mv data/secondary data/primary`
* start the (new) primary instance: `docker-compose -f primary.yml -p primary up -d`

---

### Extra

#### Existing Nextcloud

* Migrating data to S3 is tricky but possible: https://pedal.me.uk/migrating-a-nextcloud-instance-to-amazon-s3/