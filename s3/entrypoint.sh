#!/bin/sh
mc config host add master http://primary_s3_1:9000 $MINIO_ACCESS_KEY $MINIO_SECRET_KEY
mc config host add slave http://secondary_s3_1:9000 $MINIO_ACCESS_KEY $MINIO_SECRET_KEY
(mc ls master/nextcloud || mc mb master/nextcloud)
(mc ls slave/nextcloud || mc mb slave/nextcloud)
exec mc mirror --watch --remove --overwrite master/nextcloud slave/nextcloud
